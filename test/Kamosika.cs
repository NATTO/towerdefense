using UnityEngine;
public class StateBase
{
    public virtual void Start()
    {
    }
    public virtual void Dispose()
    {
    }
}
public class StartState : StateBase
{
    private GameObject ui;
    private Kamosika parent;
    public StartState(Kamosika parent)
    {
        this.parent = parent;
    }
    public override void Start()
    {
        base.Start();
        ui = GameObject.Instantiate((GameObject)Resources.Load("UIStart"));
    }
    public void OnButtonPress�Ȃ�()
    {
        parent.ChangeState(new NextState());
    }
    public override void Dispose()
    {
        base.Dispose();
        GameObject.Destroy(ui);
    }
}
public class NextState : StateBase
{
}
public class Kamosika : MonoBehaviour
{
    private StateBase nowState;
    private void Start()
    {
        ChangeState(new StartState(this));
    }
    public void ChangeState(StateBase state)
    {
        if (nowState != null)
        {
            nowState.Dispose();
        }
        nowState = state;
        if (nowState != null)
        {
            nowState.Start();
        }
    }
}