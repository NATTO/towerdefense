﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UnitDataButton : MonoBehaviour
{
    [SerializeField]
    UnitData _UnitData;

    public UnitData UnitData { get => _UnitData; set => _UnitData = value; }

    public UnitData InstanceUnitData() => Instantiate(_UnitData);

    [SerializeField]
    Image _ButtonImage;
    public Image ButtonImage => _ButtonImage;
}