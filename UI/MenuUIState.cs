using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public interface IMenuUIState
{
    IObservable<Unit> DisposeObservable { get; }
    void SetUp(UIController controller);
    void Dispose();
}
