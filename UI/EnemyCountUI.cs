using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UniRx;

public class EnemyCountUI : MonoBehaviour,IStageStartInitializable
{
    const string EnemyCountTextBase = "�G�̐� {0}/{1}";

    [SerializeField]
    TextMeshProUGUI _EnemyCountText;

    [SerializeField]
    EnemyGenerateProgress _EnemyGenerateProgress;

    public void StageStartInitialize()
    {
        _EnemyCountText.text = string.Format(EnemyCountTextBase, 0, _EnemyGenerateProgress.EnemyCount);

        _EnemyGenerateProgress.DeathEnemy.Subscribe(count => _EnemyCountText.text = string.Format(EnemyCountTextBase, count, _EnemyGenerateProgress.EnemyCount));
    }
}
