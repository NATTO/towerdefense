﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;

public class UnitSelectUI : MonoBehaviour, IMenuUIState
{
    [SerializeField]
    UnitSelectButtonUIAlignment _AlignmentUnits;

    Subject<Unit> _DisposeObservable = new Subject<Unit>();
    public IObservable<Unit> DisposeObservable => _DisposeObservable;

    Subject<UnitData> _UnitDataSelectSubject = new Subject<UnitData>();
    public IObservable<UnitData> UnitDataSelectObservable => _UnitDataSelectSubject;

    UIController _UIController;
    UnitData _SelectData;

    IMenuUIState _FormerState;

    public void Dispose()
    {
        gameObject.SetActive(false);
        _AlignmentUnits.Dispose();
        _DisposeObservable.OnNext(Unit.Default);
    }

    public void SetUp(UIController controller)
    {
        _UIController = controller;
        gameObject.SetActive(true);
        _AlignmentUnits.Initialize(null);
        _AlignmentUnits.ButtonAlignment(CharacterSelect);
    }
    public void SetFormerState(IMenuUIState state) => _FormerState = state;

    void CharacterSelect(UnitData unitData,int index)
    {
        _SelectData = unitData;
    }

    public void SelectDecide()
    {
        _UnitDataSelectSubject.OnNext(_SelectData);
        _UIController.ChangeState(_FormerState);
    }
}