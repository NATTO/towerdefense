using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UniRx;

public class PlayerHelthUI : MonoBehaviour,IStageStartInitializable
{
    const string _HelthText = "�������イ : {0}";
    [SerializeField]
    PlayerHelthManager _HelthManager;

    [SerializeField]
    TextMeshProUGUI _textMeshProUGUI;

    public int Priority = 1;
    public void StageStartInitialize()
    {
        _textMeshProUGUI.text = string.Format(_HelthText, _HelthManager.Helth.Value);
        _HelthManager.Helth.Subscribe(helth => _textMeshProUGUI.text = string.Format(_HelthText, helth));
    }
}
