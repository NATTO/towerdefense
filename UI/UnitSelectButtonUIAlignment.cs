using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UniRx;
using System.Linq;
using System.Reflection;
using TNRD;

public class UnitSelectButtonUIAlignment : MonoBehaviour
{
    enum AlignmentDirection { Horizontal, Vertical }
    [SerializeField]
    AlignmentDirection _Direction;
    [SerializeField]
    UnitOrganization _Organization;
    [SerializeField]
    Button _ButtonBase;
    [SerializeField]
    RectTransform _ParentTransform;
    [SerializeField]
    Button _DecideButton;
    [SerializeField]
    Image _DecideImage;
    [SerializeField]
    SerializableInterface<IMenuUIState> _NextUI;
    [SerializeField, ReadOnly]
    int _ButtonCount = 0;
    [SerializeField]
    Vector2 _Offset;
    [SerializeField]
    float _HorizontalDistance;
    [SerializeField]
    float _VerticalDistance;
    [SerializeField]
    List<Button> _InstanceButtons = new List<Button>();
    RectTransform _RectTransform;
    Vector2 _Size;

    public void Initialize(UnitOrganization unitOrganization)
    {
        _RectTransform = GetComponent<RectTransform>();
        _Size = _RectTransform.sizeDelta;
        if (unitOrganization != null)
            _Organization = unitOrganization;
        _ButtonCount = _Organization.Units.Count;
    }

    public void ButtonAlignment(Action<UnitData, int> buttonPushAction = null)
    {
        switch (_Direction)
        {
            case AlignmentDirection.Horizontal:
                ButtonCreateAlignmentHorizontal(buttonPushAction);
                break;
            case AlignmentDirection.Vertical:
                ButtonCreateAlignmentVertical(buttonPushAction);
                break;
        }
    }

    void ButtonCreateAlignmentHorizontal(Action<UnitData, int> buttonPushAction = null)
    {
        if (_InstanceButtons.Any()) { _InstanceButtons.ForEach(x => Destroy(x.gameObject)); _InstanceButtons.Clear(); }
        int heightCount = 0;
        int widthCount = 0;

        var buttons = CreateAlignmentButton(buttonPushAction);
        foreach (var button in buttons)
        {
            if (GetPosition(button.rectTransform.sizeDelta, widthCount, heightCount).x >= _RectTransform.sizeDelta.x)
            {
                heightCount--;
                widthCount = 0;
            }
            button.rectTransform.anchoredPosition = GetPosition(button.rectTransform.sizeDelta, widthCount, heightCount);
            _InstanceButtons.Add(button.button);

            widthCount++;
        }
    }

    void ButtonCreateAlignmentVertical(Action<UnitData, int> buttonPushAction = null)
    {
        if (_InstanceButtons.Any()) { _InstanceButtons.ForEach(x => Destroy(x.gameObject)); _InstanceButtons.Clear(); }

        var buttons = CreateAlignmentButton(buttonPushAction);
        int heightCount = 0;
        int widthCount = 0;

        foreach (var button in buttons)
        {
            if (GetPosition(button.rectTransform.sizeDelta, widthCount, heightCount).y <= -_RectTransform.sizeDelta.y)
            {
                heightCount = 0;
                widthCount++;
            }
            button.rectTransform.anchoredPosition = GetPosition(button.rectTransform.sizeDelta, widthCount, heightCount);
            _InstanceButtons.Add(button.button);

            heightCount--;
        }
    }

    List<(Button button, RectTransform rectTransform)> CreateAlignmentButton(Action<UnitData, int> buttonPushAction = null)
    {
        var results = new (Button button, RectTransform rectTransform)[_ButtonCount];

        for (int index = 0; index < _ButtonCount; index++)
        {
            var i = index;
            Button instance = Instantiate(_ButtonBase, _ParentTransform);
            UnitDataButton dataButton = instance.GetComponent<UnitDataButton>();
            RectTransform instanceTransform = instance.GetComponent<RectTransform>();
            dataButton.UnitData = _Organization.Units[index];
            instance.image.sprite = _Organization.Units[index].Status.UnitSprite;
            if (buttonPushAction != null)
                instance.onClick.AddListener(() =>
                {
                    _DecideButton?.gameObject.SetActive(true);
                    if (_DecideImage != null)
                    {
                        _DecideImage.gameObject.SetActive(true);
                        _DecideImage.rectTransform.position = instance.transform.position;
                    }
                    buttonPushAction.Invoke(dataButton.UnitData, i);
                });
            results[index] = (instance, instanceTransform);
        }

        return results.ToList();
    }

    public void Dispose()
    {
        _InstanceButtons.ForEach(button => Destroy(button.gameObject));
        _InstanceButtons.Clear();
    }

    Vector2 GetPosition(Vector2 sizeDelta, int widthCount, int heightCount)
    {
        Vector2 position = _Offset + new Vector2(sizeDelta.x / 2, -sizeDelta.y / 2)
            + Vector2.right * (sizeDelta.x + _HorizontalDistance) * widthCount
            + Vector2.up * (sizeDelta.y + _VerticalDistance) * heightCount;
        return position;
    }
}
