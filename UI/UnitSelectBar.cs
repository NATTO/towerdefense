using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 編成したプレイヤーをボタンで並べるクラス
/// </summary>
public class UnitSelectBar : MonoBehaviour, IStageStartInitializable,IUnitOrganizationInitializable
{
    [SerializeField]
    UnitOrganization _Organization;

    [SerializeField]
    UnitDataButton _UnitSelectBase;

    [SerializeField]
    Transform _SelectBarParent;

    List<UnitDataButton> _UnitDataButtons = new List<UnitDataButton>();

    public void DeleteDataButton(UnitDataButton button)
    {
        _UnitDataButtons.Remove(button);
        Destroy(button.gameObject);
        ButtonsAlignment();
    }

    public void Initialize(UnitOrganization organization)
    {
        _Organization = organization;
    }

    public void StageStartInitialize()
    {
        foreach(var unit in _Organization.Units)
        {
            var instanceButton = Instantiate(_UnitSelectBase, _SelectBarParent);
            instanceButton.ButtonImage.sprite = unit.Status.UnitSprite;
            instanceButton.UnitData = unit;
            _UnitDataButtons.Add(instanceButton);
        }

        ButtonsAlignment();
    }
    void ButtonsAlignment()
    {
        int num = 0;
        _UnitDataButtons.ForEach(unit =>
        {
            unit.transform.position = _SelectBarParent.transform.position + Vector3.right * num * unit.ButtonImage.rectTransform.sizeDelta.x;
            num++;
        });
    }
}
