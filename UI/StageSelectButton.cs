﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectButton : MonoBehaviour
{
    [SerializeField]
    Image _BackgroundImage;
    public Image BackgroundImage => _BackgroundImage;
    [SerializeField]
    Button _SelectButton;
    public Button SelectButton => _SelectButton;
    [SerializeField]
    TextMeshProUGUI _StageNameText;
    public TextMeshProUGUI StageNameText => _StageNameText;
    [SerializeField]
    RectTransform _RectTransform;
    public RectTransform RectTransform => _RectTransform;
    [SerializeField]
    ScriptableStageSettings _StageSettings;
    public ScriptableStageSettings StageSettings { get => _StageSettings; set => _StageSettings = value; }
}