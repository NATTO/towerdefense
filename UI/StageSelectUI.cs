using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TNRD;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectUI : MonoBehaviour, IMenuUIState
{
    [SerializeField]
    List<ScriptableStageSettings> _StageSettings = new List<ScriptableStageSettings>();

    [SerializeField]
    StageSelectButton _SelectStageButtonBase;

    [SerializeField]
    Image _StageImage;

    [SerializeField]
    TextMeshProUGUI _StageDescriptionText;

    [SerializeField]
    SerializableInterface<IMenuUIState> _NextUI;

    [SerializeField]
    RectTransform _StageButtonParent;

    [SerializeField]
    float _StageButtonHeightDistance;

    [SerializeField]
    Vector2 _Offset;

    StageSelectButton[] _InstanceButtons;

    UIController _UIController;

    Subject<Unit> _DisposeSubject = new Subject<Unit>();
    public IObservable<Unit> DisposeObservable => _DisposeSubject;

    public void Dispose()
    {
        _DisposeSubject.OnNext(Unit.Default);
        gameObject.SetActive(false);
        _StageButtonParent.gameObject.SetActive(false);
        foreach(var instanceButton in _InstanceButtons)
            Destroy(instanceButton.gameObject);
    }

    public void SetUp(UIController controller)
    {
        _UIController = controller;
        gameObject.SetActive(true);
        _StageButtonParent.gameObject.SetActive(true);
        StageButtonAlignment();
    }

    public void StageButtonAlignment()
    {
        Vector2 position = _Offset;
        _InstanceButtons = new StageSelectButton[_StageSettings.Count];
        int index = 0;
        foreach (var stageSetting in _StageSettings)
        {
            var instanceButton = Instantiate(_SelectStageButtonBase, _StageButtonParent);

            instanceButton.StageNameText.text = stageSetting.StageName;
            instanceButton.SelectButton.onClick.AddListener(() => StageSelect(instanceButton.StageSettings));
            instanceButton.RectTransform.anchoredPosition = position;
            instanceButton.StageSettings = stageSetting;
            position.y -= instanceButton.RectTransform.sizeDelta.y + _StageButtonHeightDistance;
            _InstanceButtons[index] = instanceButton;
            index++;
        }
    }

    public void StageSelect(ScriptableStageSettings setting)
    {
        if(GameSettings.StageSettings != null && GameSettings.StageSettings == setting)
            _UIController.ChangeState(_NextUI.Value);

        _StageDescriptionText.text = setting.StageDescription;
        _StageImage.sprite = setting.StageMapPicture;

        GameSettings.StageSettings = setting;
    }
}
