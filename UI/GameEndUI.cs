﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using TMPro;

public class GameEndUI : MonoBehaviour,IStageStartInitializable
{
    const string _GameOverText = "ゲームオーバー";
    const string _GameClearText = "ゲームクリアー";
    [SerializeField]
    StageManager _StageManager;
    [SerializeField]
    TextMeshProUGUI _GameEndText;

    public void StageStartInitialize()
    {
        _GameEndText.gameObject.SetActive(false);

        _StageManager.GameClearObservable.Subscribe(_ =>
        {
            _GameEndText.gameObject.SetActive(true);
            SetGameEndText(_GameClearText);
        });
        _StageManager.GameOverObservable.Subscribe(_ =>
        {
            _GameEndText.gameObject.SetActive(true);
            SetGameEndText(_GameOverText);
        });
    }

    void SetGameEndText(string text)
    {
        _GameEndText.text = text;
    }
}