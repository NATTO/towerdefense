﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

class SelectOrganization : MonoBehaviour, IMenuUIState
{
    [SerializeField]
    UnitSelectButtonUIAlignment _OrganizationButtonUI;
    [SerializeField]
    UnitSelectUI _PossessionUnitSelect;
    [SerializeField]
    UnitOrganization _Organization;
    public UnitOrganization Organization => _Organization;

    UIController _Controller;

    Subject<Unit> _DisposeObservable = new Subject<Unit>();
    public IObservable<Unit> DisposeObservable => _DisposeObservable;

    public void Dispose()
    {
        _DisposeObservable.OnNext(Unit.Default);
        _OrganizationButtonUI.Dispose();
        gameObject.SetActive(false);
    }

    public void LoadPossessionCharacter(int selectIndex)
    {
        _Controller.ChangeState(_PossessionUnitSelect);
        _PossessionUnitSelect.SetFormerState(this);
        _PossessionUnitSelect.UnitDataSelectObservable.First().Subscribe(data =>
        {
            _Organization.Units[selectIndex] = data;
        });
    }

    public void SetUp(UIController controller)
    {
        _Controller = controller;
        gameObject.SetActive(true);
        _OrganizationButtonUI.Initialize(_Organization);
        _OrganizationButtonUI.ButtonAlignment((data,index) =>
        {
            LoadPossessionCharacter(index);
        });
    }
}