using TNRD;
using UnityEngine;

public class UIController : SingletonMonoBehaviour<UIController>
{
    IMenuUIState _NowMenuUIState;
    [SerializeField]
    SerializableInterface<IMenuUIState> _StartState;

    protected override void Awake()
    {
        base.Awake();
        ChangeState(_StartState.Value);
    }
    public void ChangeState(IMenuUIState nextState)
    {
        if (_NowMenuUIState != null)
        {
            _NowMenuUIState.Dispose();
        }
        _NowMenuUIState = nextState;
        if (_NowMenuUIState != null)
        {
            _NowMenuUIState.SetUp(this);
        }
    }
}
