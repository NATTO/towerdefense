using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartButton : MonoBehaviour
{
    [SerializeField]
    SelectOrganization _Organization;

    /// <summary>
    /// 編成とか設定とか入れてゲームシーンに飛ぶやつ
    /// </summary>
    public void GameSceneLoad()
    {
        GameSettings.Organization = _Organization.Organization;
        SceneLoadManager.Instance.LoadSceneAsync(SceneNames.GameScene);
    }
}
