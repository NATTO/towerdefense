using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using TMPro;

public class CostUI : MonoBehaviour,IStageStartInitializable
{
    const string CostTextBase = "�R�X�g : {0}";

    [SerializeField]
    TextMeshProUGUI _CostText;

    [SerializeField]
    CostManager _CostManager;

    public void StageStartInitialize()
    {
        _CostText.text = string.Format(CostTextBase, 0);
        _CostManager.CostReactiveProperty.Subscribe(cost =>
        {
            _CostText.text = string.Format(CostTextBase, cost);
        });
    }
}
