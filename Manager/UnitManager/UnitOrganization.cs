using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ユニット編成
/// </summary>
[CreateAssetMenu(menuName = "Unit/Organization")]
public class UnitOrganization : ScriptableObject
{
    [SerializeField]
    List<UnitData> _Units = new List<UnitData>();
    public List<UnitData> Units => _Units;
}
