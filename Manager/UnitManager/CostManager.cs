﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UniRx;
using UnityEngine;
/// <summary>
/// コスト管理
/// </summary>
public class CostManager : MonoBehaviour, IStageSettingInitializable,IStageStartInitializable
{
    [SerializeField, ReadOnly]
    ReactiveProperty<int> _Cost = new ReactiveProperty<int>();

    [SerializeField, ReadOnly]
    int _CostRecoveryTime;

    [SerializeField, ReadOnly]
    int _RecoveryCostPerTime;

    CancellationTokenSource _DestroyCancellationTokenSource = new CancellationTokenSource();

    public ReactiveProperty<int> CostReactiveProperty => _Cost;

    public void CostRecovery(int value) => _Cost.Value += value;

    public void CostUse(int value) => _Cost.Value -= value;

    public bool IsCostUse(int useCost) => _Cost.Value >= useCost;

    public void Initialize(ScriptableStageSettings settings)
    {
        _Cost.Value = settings.StartCost;
        _CostRecoveryTime = settings.CostRecoveryTime;
        _RecoveryCostPerTime = settings.RecoveryCostPerTime;
    }

    public void StageStartInitialize()
    {
        CostRecoveryTask(_DestroyCancellationTokenSource.Token);
    }

    public void GameStop()
    {

    }

    public void GameEnd()
    {
        _DestroyCancellationTokenSource.Cancel();
    }
    /// <summary>
    /// コストの時間経過回復
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTask CostRecoveryTask(CancellationToken cancellationToken)
    {
        while (true)
        {
            await UniTask.Delay(_CostRecoveryTime, false, PlayerLoopTiming.Update, cancellationToken);
            CostRecovery(_RecoveryCostPerTime);
        }
    }

}