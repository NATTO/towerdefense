using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;
using TNRD;
/// <summary>
/// ユニット選択用クラス
/// </summary>
public class UnitSelecter : MonoBehaviour
{
    UnitData _UnitData;

    [SerializeField]
    SerializableInterface<IInputable> _Input;
    IInputable _RantimeInput;
    [SerializeField]
    CostManager _CostManager;
    [SerializeField]
    UnitSelectBar _UnitSelectBar;
    [SerializeField,ReadOnly]
    UnitDataButton _SelectUnitDataButton;


    private void Start()
    {
        _RantimeInput = _Input.Value;
        //クリック押下
        _RantimeInput.InputDown.Subscribe(_ =>
        {
            _SelectUnitDataButton = null;
            _RantimeInput.UIResults.FirstOrDefault(value => value.gameObject.TryGetComponent(out _SelectUnitDataButton));
            //ユニット用のボタンがあったら
            if (_SelectUnitDataButton != null)
            {
                //コスト使えなかったら帰る
                if (_CostManager.IsCostUse(_SelectUnitDataButton.UnitData.Status.Cost) == false)
                    return;
                //ユニット生成
                _UnitData = _SelectUnitDataButton.InstanceUnitData();
            }
        });
        //クリック押中
        _RantimeInput.InputStay.Subscribe(_ =>
        {
            if (_UnitData == null) return;
            //ユニット移動
            _UnitData.transform.position = _RantimeInput.MousePointerRayPosition;
        });
        //クリック押上
        _RantimeInput.InputUp.Subscribe(_ =>
        {
            //ユニット持ってなかったら帰る
            if (_UnitData == null) return;
            //ユニットスタンド押してたら
            if (_RantimeInput.GameObjectResult.tag == TagNames.UnitStand)
            {
                UnitStand stand = _RantimeInput.GameObjectResult.GetComponent<UnitStand>();
                //もうユニットあったら壊す
                if (stand.Unit != null)
                {
                    Destroy(_UnitData.gameObject);
                    return;
                }
                //ユニット初期化
                var controller = _UnitData.GetComponent<ICharactorControllable>();
                controller.Initialize();
                //スタンドにユニットくっつける
                stand.SetUnit(_UnitData.transform, _UnitData.gameObject);
                //コスト使う
                _CostManager.CostUse(_UnitData.Status.Cost);
                //ユニット動き出す
                controller.OperationStart();
                //ボタン消す
                _UnitSelectBar.DeleteDataButton(_SelectUnitDataButton);
                _UnitData = null;
            }
            //押してなかったら作ったやつ壊す
            else Destroy(_UnitData.gameObject);
        });
    }
}
