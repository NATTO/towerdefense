using System.Collections;
using System.Collections.Generic;
using TMPro;
using TNRD;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Linq;
using UnityEngine.EventSystems;

public class UnitWithdrawal : MonoBehaviour, IStageStartInitializable
{
    static readonly RaycastResult defaultRaycastResult = new RaycastResult();
    [SerializeField]
    SerializableInterface<IInputable> _Input;
    [SerializeField]
    Button _WithdrawalButton;
    IInputable _RantimeInput;
    UnitStand _SelectStand;
    ICharactorControllable _Data;

    public void StageStartInitialize()
    {
        _RantimeInput = _Input.Value;

        _RantimeInput.InputDown.Subscribe(_ =>
        {
            if (_RantimeInput.GameObjectResult.CompareTag(TagNames.UnitStand))
            {
                var selctStand = _RantimeInput.GameObjectResult.GetComponent<UnitStand>();
                if (_SelectStand == selctStand) return;
                if (selctStand != _SelectStand) _WithdrawalButton.onClick.RemoveListener(Withdrawal);

                _SelectStand = selctStand;
                _Data = _SelectStand.Unit.GetComponent<ICharactorControllable>();
                _WithdrawalButton.onClick.AddListener(Withdrawal);
                _WithdrawalButton.gameObject.SetActive(true);
            }
            else if (_Data != null && _RantimeInput.UIResults.FirstOrDefault(ui => ui.gameObject == _WithdrawalButton.gameObject).ToString() == defaultRaycastResult.ToString())
            {
                _WithdrawalButton.onClick.RemoveListener(Withdrawal);
                _WithdrawalButton.gameObject.SetActive(false);
                _Data = null;
                _SelectStand = null;
            }
        });
    }

    void Withdrawal()
    {
        _Data.OperationFinish();
        _Data = null;
        _SelectStand = null;
        _WithdrawalButton.gameObject.SetActive(false);
        _WithdrawalButton.onClick.RemoveListener(Withdrawal);
    }
}
