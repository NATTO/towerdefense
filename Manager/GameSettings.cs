﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
static class GameSettings
{
    public static UnitOrganization Organization { get; set; }
    public static ScriptableStageSettings StageSettings { get; set; }
}
