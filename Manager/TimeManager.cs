using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 時間まねーじゃー
/// </summary>
public static class TimeManager

{
    static float _InGameTimeScale = 1f;
    public static float InGameTimeScale { get { return _InGameTimeScale; } set { _InGameTimeScale = value;} }
}
