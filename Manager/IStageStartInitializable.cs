﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
interface IStageStartInitializable
{
    int Priority() => 0;
    void StageStartInitialize();
}