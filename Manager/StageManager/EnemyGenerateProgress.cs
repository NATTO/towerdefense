using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TNRD;
using UniRx;
using UnityEngine;

/// <summary>
/// 敵の生成管理
/// </summary>
public class EnemyGenerateProgress : MonoBehaviour,IStageSettingInitializable
{
    [SerializeField, ReadOnly]
    GenerateEnemyData _GenerateData;
    [SerializeField, ReadOnly]
    int _EnemyCount;
    public int EnemyCount => _EnemyCount;
    [SerializeField, ReadOnly]
    ReactiveProperty<int> _DeathEnemy;
    public ReactiveProperty<int> DeathEnemy => _DeathEnemy;
    [SerializeField, ReadOnly]
    ReactiveProperty<int> _MoveEndEnemy;
    [SerializeField, ReadOnly]
    bool _IsEnd;
    [SerializeField]
    bool _DebugIsGenerate;

    GenerateEnemyData _RantimeGenerateData;

    [SerializeField, ReadOnly]
    List<ICharactorControllable> _Enemies;

    Subject<Unit> GameEndSubject;

    public IObservable<Unit> GameEndObservable => GameEndSubject;

    CancellationTokenSource _GenerateToken;

    void OnDestroy() => _GenerateToken.Cancel();
    public void Initialize(ScriptableStageSettings settings)
    {
        _GenerateToken = new CancellationTokenSource();
        GameEndSubject = new Subject<Unit>();
        _GenerateData = settings.GenerateEnemyData;
        int count = _GenerateData.Enemies.Count;
        _RantimeGenerateData = Instantiate(_GenerateData);
        var nextGenerateData = _RantimeGenerateData.NextEnemyData;

        while (nextGenerateData != null)
        {
            count += nextGenerateData.Enemies.Count;
            nextGenerateData = nextGenerateData.NextEnemyData == null ? null : nextGenerateData.NextEnemyData;
        }
        _EnemyCount = count;
        _Enemies = new List<ICharactorControllable>(_EnemyCount);

        GameEndSubject.First().Subscribe(_ => _IsEnd = true);
    }

    public void Initialize()
    {
    }

    public void GenerateStart()
    {
        EnemyGenerateStart(_GenerateToken.Token);
    }

    public void AllEnemyStop()
    {
        _Enemies.ForEach(x => x.OperationStop());
    }

    /// <summary>
    /// 敵の生成を終わらせる、それは再生できない
    /// </summary>
    public void GenerateEnd()
    {
        _GenerateToken.Cancel();
    }

    /// <summary>
    /// 生成進行
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTask EnemyGenerateStart(CancellationToken cancellationToken)
    {
        GenerateEnemyData nextGenerateData = _RantimeGenerateData.NextEnemyData == null ? null : Instantiate(_RantimeGenerateData.NextEnemyData);
        await UniTask.Delay(_RantimeGenerateData.GenerateWaitTime, false, PlayerLoopTiming.Update, cancellationToken);
        Generate(_RantimeGenerateData.Enemies);

        while (nextGenerateData != null)
        {
            float elapsedTime = 0;
            while (elapsedTime * 1000 <= nextGenerateData.GenerateWaitTime)
            {
                await UniTask.WaitForFixedUpdate(cancellationToken);

                elapsedTime += Time.fixedDeltaTime;
            }
            while(_DebugIsGenerate == false) await UniTask.Yield();

            Generate(nextGenerateData.Enemies);
            nextGenerateData = nextGenerateData.NextEnemyData != null ? Instantiate(nextGenerateData.NextEnemyData) : null;
        }
        //敵を生成
        void Generate(List<SerializableInterface<ICharactorControllable>> enemies)
        {
            foreach (var enemy in enemies)
            {
                enemy.Reference = Instantiate(enemy.Reference);

                enemy.Value.Initialize();
                enemy.Value.OperationStart();
                //役割が終わった時の処理
                enemy.Value.OperationFinishObservable().First().Subscribe(_ =>
                {
                    if (enemy.Reference.TryGetComponent<IEnemyMovable>(out var movable))
                    {
                        if (movable.IsMoveEnd)
                            _MoveEndEnemy.Value++;
                    }
                    if (enemy.Reference.TryGetComponent<IDamagable>(out var damegable))
                    {
                        if (damegable.IsDeath)
                            _DeathEnemy.Value++;
                    }
                    if (_DeathEnemy.Value + _MoveEndEnemy.Value == _EnemyCount)
                        GameEndSubject.OnNext(Unit.Default);

                }).AddTo(cancellationToken);
                _Enemies.Add(enemy.Value);
            }
        }
    }
}
