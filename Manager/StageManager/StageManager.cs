﻿using System.Collections;
using UnityEngine;
using UniRx;
using System.Linq;
using System;
/// <summary>
/// ステージ自体を管理
/// </summary>
public class StageManager : MonoBehaviour
{
    [SerializeField]
    EnemyGenerateProgress _EnemyGenerateProgress;

    [SerializeField]
    PlayerHelthManager _PlayerHelthManager;

    [SerializeField]
    ScriptableStageSettings _StageSettings;

    Subject<Unit> _GameClearSubject = new Subject<Unit>();
    public IObservable<Unit> GameClearObservable => _GameClearSubject;
    Subject<Unit> _GameOverSubject = new Subject<Unit>();
    public IObservable<Unit> GameOverObservable => _GameOverSubject;

    private void Awake()
    {
        _EnemyGenerateProgress.Initialize();
        foreach (var initComponent in GetComponents<IStageSettingInitializable>())
        {
            initComponent.Initialize(_StageSettings);
        }
    }
    /// <summary>
    /// ゲームスタート
    /// </summary>
    public void GameStart()
    {
        //敵の生成開始
        _EnemyGenerateProgress.GenerateStart();
        _EnemyGenerateProgress.GameEndObservable.First().Subscribe(_ =>
        {
            //プレイヤーの耐久が尽きず敵が全員消えたらクリア
            GameClear();
        }).AddTo(gameObject);

        //プレイヤーの耐久が尽きたらゲームオーバー
        _PlayerHelthManager.PlayerDeathObservable.First().Subscribe(_ =>
        {
            _EnemyGenerateProgress.AllEnemyStop();
            _EnemyGenerateProgress.GenerateEnd();
            GameOver();
        }).AddTo(gameObject);

        foreach (var stageStart in GameObjectExtensions.FindObjectsOfInterface<IStageStartInitializable>().OrderBy(x => x.Priority()))
        {
            stageStart.StageStartInitialize();
        }
    }

    void GameClear()
    {
        _GameClearSubject.OnNext(Unit.Default);
    }

    void GameOver()
    {
        _GameOverSubject.OnNext(Unit.Default);
    }
}