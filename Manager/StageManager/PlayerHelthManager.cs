using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
/// <summary>
/// �v���C���[�̑ϋv�Ǘ�
/// </summary>
public class PlayerHelthManager : MonoBehaviour,IStageSettingInitializable
{
    /// <summary>
    /// �ϋv
    /// </summary>
    [SerializeField, ReadOnly]
    ReactiveProperty<int> _Helth;

    Subject<Unit> _PlayerDeathSubject = new Subject<Unit>();

    public IObservable<Unit> PlayerDeathObservable => _PlayerDeathSubject;

    public ReactiveProperty<int> Helth => _Helth;

    public void Initialize(ScriptableStageSettings stageSettings)
    {
        _Helth = new ReactiveProperty<int>(stageSettings.PlayerHelth);
        var arrivalPoints = FindObjectsOfType<EnemyArrivalPoint>();
        
        foreach (var point in arrivalPoints)
        {
            point.ArraivalEvent.AddListener(EnemyArrival);
        }
    }

    /// <summary>
    /// �G���S�[���ɓ��B�������̏���
    /// </summary>
    void EnemyArrival()
    {
        _Helth.Value--;
        if(_Helth.Value == 0)
        {
            _PlayerDeathSubject.OnNext(Unit.Default);
        }
    }
}
