using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ステージの設定
/// </summary>
[CreateAssetMenu(menuName = "Stage/StageSetting")]
public class ScriptableStageSettings : ScriptableObject
{
    /// <summary>
    /// プレイヤーの耐久
    /// </summary>
    public int PlayerHelth;
    /// <summary>
    /// 最初のコスト
    /// </summary>
    public int StartCost;
    /// <summary>
    /// コストが回復するまで
    /// </summary>
    public int CostRecoveryTime = 1000;
    /// <summary>
    /// 一度に回復するコスト
    /// </summary>
    public int RecoveryCostPerTime = 1;
    /// <summary>
    /// 生成する敵のデータ
    /// </summary>
    public GenerateEnemyData GenerateEnemyData;
    /// <summary>
    /// ステージのマップ
    /// </summary>
    public GameObject StageMap;
    /// <summary>
    /// マップの全体図
    /// </summary>
    public Sprite StageMapPicture;
    /// <summary>
    /// ステージの名前
    /// </summary>
    public string StageName;
    /// <summary>
    /// ステージの説明
    /// </summary>
    [Multiline(10)]
    public string StageDescription;
}
