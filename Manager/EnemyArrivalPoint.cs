using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// 敵の到達点
/// </summary>
public class EnemyArrivalPoint : MonoBehaviour
{
    UnityEvent _ArrivalEvent = new UnityEvent();

    public UnityEvent ArraivalEvent => _ArrivalEvent;

    Dictionary<ICharactorControllable, IDisposable> charactorControllables = new Dictionary<ICharactorControllable, IDisposable>();

    private void OnTriggerEnter(Collider other)
    {
        //敵が触れた時
        if (other.TryGetComponent<ICharactorControllable>(out var controll) == true)
        {
            //敵が自分の中で移動終わったら
            charactorControllables.Add(controll, controll.ObserveEveryValueChanged(controll => controll.IsOperationEnd).Where(isEnd => isEnd == true).First().Subscribe(x =>
            {
                //移動終わったっぽい発行
                _ArrivalEvent?.Invoke();
            }));
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //敵が離れたら
        if (other.TryGetComponent<ICharactorControllable>(out var controll) == true)
        {
            //『無かったことにする』
            charactorControllables[controll].Dispose();
            charactorControllables.Remove(controll);
        }
    }
}
