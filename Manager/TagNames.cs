using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// タグの名前管理
/// </summary>
public static class TagNames
{
    public static string UnitStand = "UnitStand";
    public static string Enemy = "Enemy";
}
