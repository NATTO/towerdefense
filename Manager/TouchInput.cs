using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
/// <summary>
/// マウスクリックとPCクリックやら
/// </summary>
public class TouchInput : SingletonMonoBehaviour<TouchInput>, IInputable
{
    Touch[] touches = new Touch[2] { default, default };
    public Touch[] Touches => touches;

    PointerEventData firstPointData;
    PointerEventData secondPointData;
    List<RaycastResult> firstResults = new List<RaycastResult>();
    List<RaycastResult> secondResults = new List<RaycastResult>();
    EventSystem currentEventSystem = null;
    Camera mainCamera;
    Subject<Unit> _InputDown;
    Subject<Unit> _InputStay;
    Subject<Unit> _InputUp;
    [SerializeField, ReadOnly]
    List<RaycastResult> _UIResults;
    [SerializeField, ReadOnly]
    GameObject _GameObjectResult;
    [SerializeField, ReadOnly]
    Vector3 _MousePointerRayPosition;

    public Subject<Unit> InputDown => _InputDown;

    public Subject<Unit> InputUp => _InputUp;

    public Subject<Unit> InputStay => _InputStay;

    public IReadOnlyList<RaycastResult> UIResults => _UIResults;

    public GameObject GameObjectResult => _GameObjectResult;

    public Vector3 MousePointerRayPosition => _MousePointerRayPosition;

    protected override void Awake()
    {
        base.Awake();
        mainCamera = Camera.main;
        currentEventSystem = EventSystem.current;
        firstPointData = new PointerEventData(currentEventSystem);
        secondPointData = new PointerEventData(currentEventSystem);
        _InputDown = new Subject<Unit>();
        _InputUp = new Subject<Unit>();
        _InputStay = new Subject<Unit>();
    }

    void Update()
    {

#if !UNITY_EDITOR && UNITY_ANDROID
        if (Input.touchCount == 0) return;

        firstResults.Clear();
        secondResults.Clear();
        touches[0] = Input.GetTouch(0);
        firstPointData.position = touches[0].position;
        currentEventSystem.RaycastAll(firstPointData, firstResults);

        if (Input.touchCount > 1)
        {
            touches[1] = Input.GetTouch(1);
            secondPointData.position = touches[1].position;
            currentEventSystem.RaycastAll(secondPointData, secondResults);
        }
        else touches[1] = default;

        foreach (var firstResult in firstResults)
        {
            Touch(touches[0], firstResult.gameObject.name);
        }
        foreach (var secondResult in secondResults)
        {
            Touch(touches[1], secondResult.gameObject.name);
        }

        void Touch(Touch value, string objectName)
        {

        }
#endif
#if UNITY_EDITOR


        bool mouseButtonDown = Input.GetMouseButtonDown(0);
        bool mouseButton = Input.GetMouseButton(0);
        bool mouseButtonUp = Input.GetMouseButtonUp(0);
        Vector3 mousePosition = Input.mousePosition;        

        Ray cameraRay = mainCamera.ScreenPointToRay(mousePosition);
        RaycastHit hit;
        bool isHit = Physics.Raycast(cameraRay, out hit, float.MaxValue);
        if (isHit)
        {
            _UIResults = firstResults;
            _GameObjectResult = hit.transform.gameObject;
            _MousePointerRayPosition = hit.point;
        }


        firstPointData.position = mousePosition;
        currentEventSystem.RaycastAll(firstPointData, firstResults);

        if (mouseButtonDown)
            _InputDown.OnNext(Unit.Default);
        if (mouseButton)
            _InputStay.OnNext(Unit.Default);
        if (mouseButtonUp)
            _InputUp.OnNext(Unit.Default);
#endif
    }
    GameObject d;
}
interface IInputable
{
    Subject<Unit> InputDown { get; }
    Subject<Unit> InputUp { get; }
    Subject<Unit> InputStay { get; }

    IReadOnlyList<RaycastResult> UIResults { get; }
    GameObject GameObjectResult { get; }
    Vector3 MousePointerRayPosition { get; }
}