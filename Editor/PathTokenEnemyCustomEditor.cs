using System.Collections;

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

[CustomEditor(typeof(ScriptablePathTokenEnemy))]
public class PathTokenEnemyCustomEditor : Editor
{
    ScriptablePathTokenEnemy _PathTokenEnemy;
    private void OnEnable()
    {
        _PathTokenEnemy = target as ScriptablePathTokenEnemy;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

    }

    void OnSceneGUI(SceneView sceneView)
    {
        List<TokenEnemy> tokens = new List<TokenEnemy>();
        tokens.Add(_PathTokenEnemy._StartPosition);
        tokens.AddRange(_PathTokenEnemy._OnTheWayPositions);
        tokens.Add(_PathTokenEnemy._EndPosition);

        for (int index = 0; index < tokens.Count; index++)
        {
            Vector3 nodePosition = tokens[index]._Position;

            Vector3 newNodePosition = nodePosition;

            newNodePosition = Handles.PositionHandle(newNodePosition, Quaternion.identity);
            Handles.color = Color.blue;

            if(newNodePosition != nodePosition)
            {
                Undo.RecordObject(target, "Moved Point");
                tokens[index]._Position = newNodePosition;
            }

            Handles.SphereHandleCap(0, nodePosition, Quaternion.identity, 1f, EventType.Repaint);

            Handles.Label(nodePosition, $"Node{index}");
            if(index != 0)
                Handles.DrawLine(tokens[index]._Position, tokens[index-1]._Position);
        }
    }
}
