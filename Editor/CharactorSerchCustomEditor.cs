﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CharactorSerch))]
public class CharactorSerchCustomEditor : Editor
{
    private void OnSceneGUI()
    {
        Handles.color = Color.black;
        Handles.CircleHandleCap(0, ((Component)target).transform.position, Quaternion.Euler(Vector3.right * 90), ((CharactorSerch)target).SerchDistance, EventType.Repaint);
    }
}