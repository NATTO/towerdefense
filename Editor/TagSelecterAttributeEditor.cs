﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomPropertyDrawer(typeof(TagSelecterAttribute))]
public class TagSelecterAttributeEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Type tagNameType = typeof(TagNames);
        var fields = tagNameType.GetFields().Select(x => x.GetValue(null).ToString()).ToArray();

        EditorGUI.BeginProperty(position, label, property);

        Rect labelRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        EditorGUI.LabelField(labelRect, "対象タグ");

        Rect dropdownRect = new Rect(position.x, labelRect.y, position.width, EditorGUIUtility.singleLineHeight);
        int selectIndex = Array.IndexOf(fields, property.stringValue);
        selectIndex = EditorGUI.Popup(dropdownRect, " ", selectIndex, fields);

        property.stringValue = fields[selectIndex].ToString();

        EditorGUI.EndProperty();
    }
}