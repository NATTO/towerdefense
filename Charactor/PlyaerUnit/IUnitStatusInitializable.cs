﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// ユニットの初期化インターフェース
/// ユニットである証明
/// </summary>
public interface IUnitStatusInitializable : ICharactorStatusInitializable
{

}