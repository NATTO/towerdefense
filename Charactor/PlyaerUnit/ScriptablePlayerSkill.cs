﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScriptablePlayerSkill : ScriptableObject
{
    public string SkillName;
    public string SkillDescription;
    [Tooltip("最初のスキル待機時間、1000=1秒")] public int FirstCastTime;
    [Tooltip("2度目以降の待機時間、1000=1秒")] public int ReCastTime;
    [Tooltip("一度に発動するスキル")] public List<Skill> Skills = new List<Skill>();
    [Tooltip("スキル発動中のアニメーション")] public AnimationClip AnimationClip;
    [Tooltip("スキル発動中のエフェクト")]public ParticleSystem ParticleSystem;
    [Tooltip("スキル発動中の射程距離")] public float SkillRange;
}

public enum SkillType
{
    None = 0,
    Attack = 1,
    Heal = 2,
    PhysicalAttackBuff = 3,
    PhysicalDefernceBuff = 4,
    MagicalAttackBuff = 5,
    MagicalDefernceBuff = 6,
    MaxHPBuff = 7,
    BlockCountBuff = 8,
}

public enum SkillTarget
{
    Me = 0,
    Enemy = 1,
}


public struct Skill
{
    public SkillType Type;
    public SkillTarget Target;
    /// <summary>
    /// 効果量
    /// </summary>
    public int Amost;
}