﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// ユニットの種類
/// </summary>
public enum UnitAppropriate
{
    //近距離
    Short,
    //遠距離
    Long,
    //両用
    All
}