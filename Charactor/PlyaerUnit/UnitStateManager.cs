﻿using System.Collections;
using UniRx;
using UnityEngine;

/// <summary>
/// ユニットの状態遷移管理
/// </summary>
public class UnitStateManager : MonoBehaviour,IUnitStatusInitializable
{
    [SerializeField]
    ReactiveProperty<UnitState> _State = new ReactiveProperty<UnitState>();
    public ReactiveProperty<UnitState> State => _State;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {

    }
}

public enum UnitState
{
    Idle,
    ShortRangeAttack,
    LongRangeAttack,
    SkillAttack,
    Die,
}