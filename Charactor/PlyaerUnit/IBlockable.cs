﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

public interface IBlockable
{
    Subject<Unit> BlockSubject { get; }
    IDictionary<GameObject, IEnemyUsable> BlockEnemies { get; }
    void Block(GameObject blockUnit);
    bool IsBlock { get; }
}
