﻿using UnityEngine;
/// <summary>
/// ユニットのステータス
/// </summary>
[CreateAssetMenu(menuName = "Unit/Status")]
public class ScriptableUnitStatus : ScriptableBaseCharactorStatus
{
    [SerializeField]
    int _Cost;
    public int Cost { get => _Cost; }
    [SerializeField]
    Sprite _UnitSprite;
    public Sprite UnitSprite { get => _UnitSprite; }
    [SerializeField]
    int _BlockLimit;
    public int BlockLimit { get => _BlockLimit; set => _BlockLimit = value; }

    [SerializeField]
    ScriptablePlayerSkill _PlayerSkill;
    public ScriptablePlayerSkill PlayerSkill { get => _PlayerSkill; }
}