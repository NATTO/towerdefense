﻿using System.Collections;
using UnityEngine;
using UniRx;

/// <summary>
/// ユニットのアニメーション遷移管理
/// </summary>
public class UnitAnimationManager : MonoBehaviour,IUnitStatusInitializable
{
    [SerializeField] private UnitStateManager _UnitStateManager;
    [SerializeField] private Animator _UnitAnimator;

    public int Priority => 1;
    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        _UnitStateManager.State.Subscribe(state =>
        {
            _UnitAnimator.Play(state.ToString(),0,0);
        });
    }
}