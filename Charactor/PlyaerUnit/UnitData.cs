﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitData : MonoBehaviour
{
    [SerializeField]
    UnitAppropriate _UnitAppropriate;
    public UnitAppropriate UnitAppropriate => _UnitAppropriate;

    [SerializeField]
    ScriptableUnitStatus _UnitStatus;

    public ScriptableUnitStatus Status => _UnitStatus;
}