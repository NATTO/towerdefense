﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TNRD;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UIElements;
/// <summary>
/// ユニット管理
/// </summary>
public class UnitController : MonoBehaviour, ICharactorControllable
{
    [SerializeField]
    UnitData _UnitData;
    [SerializeField]
    UnitStateManager _StateManager;
    [SerializeField]
    SerializableInterface<ICharactorAttackable> _Attackable;
    [SerializeField]
    SerializableInterface<IBlockable> _Blockable;
    [SerializeField]
    SerializableInterface<IDamagable> _Damagable;
    [SerializeField]
    SerializableInterface<ISerchable> _Serchable;


    ICharactorAttackable _RantimeAttackable;
    IBlockable _RantimeBlockable;
    IDamagable _RantimeDamagable;
    ISerchable _RantimeSerchable;
    IEnumerable<GameObject> _SerchEnemies;
    UniTask _AttackTask = UniTask.CompletedTask;
    Subject<Unit> _OperationFinishSubject = new Subject<Unit>();
    Transform _Transform;
    CancellationTokenSource _DestroyCancellationTokenSource = new();

    public bool IsOperationEnd => _RantimeDamagable.IsDeath == false;
    /// <summary>
    /// 遠距離攻撃
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTask LongRangeAttack(CancellationToken cancellationToken)
    {
        int attackTime = 0;
        //敵を探さないなら帰る
        if (_UnitData.Status.SerchDistance <= 0) return;
        while (true)
        {
            //攻撃までの待機時間
            attackTime += (int)(Time.deltaTime * 1000);
            await UniTask.Yield(cancellationToken);
            if (_SerchEnemies == null || _RantimeBlockable.BlockEnemies.Count != 0) continue;
            if (_SerchEnemies.Count() == 0 || _UnitData.Status.LongRangeAttackTime >= attackTime) continue;

            _StateManager.State.Value = UnitState.LongRangeAttack;
            _Transform.LookAt(_SerchEnemies.First().transform);
            await _RantimeAttackable.AttackTask(_SerchEnemies.Select(enemy => new KeyValuePair<GameObject, IDamagable>(enemy, enemy.GetComponent<IDamagable>())).ToList(), cancellationToken, AttackRange.Long);
            attackTime = 0;
            _StateManager.State.Value = UnitState.Idle;
        }
    }

    /// <summary>
    /// 近距離攻撃
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTask BlockAttackTask(CancellationToken cancellationToken)
    {
        //ブロックしてるやつが居なくなるまで続ける
        while (_RantimeBlockable.BlockEnemies.Count != 0)
        {
            //敵の方向を向く
            _Transform.LookAt(_RantimeBlockable.BlockEnemies.First().Key.transform);
            await UniTask.Delay(_UnitData.Status.AttackTime);
            _StateManager.State.Value = UnitState.ShortRangeAttack;
            await _RantimeAttackable.AttackTask(_RantimeBlockable.BlockEnemies.Select(enemy => new KeyValuePair<GameObject, IDamagable>(enemy.Key, enemy.Value.Helth)).ToList(), cancellationToken, AttackRange.Short);
            //死んだ敵をブロックしてる状態から外す
            var destroyEnemies = new Dictionary<GameObject, IEnemyUsable>(_RantimeBlockable.BlockEnemies.Where(enemy => enemy.Value.Helth.IsDeath));
            foreach (var blockEnemy in destroyEnemies)
            {
                _RantimeBlockable.BlockEnemies.Remove(blockEnemy.Key);
            }
            _StateManager.State.Value = UnitState.Idle;
        }
    }

    public void Initialize()
    {
        _Transform = transform;
        _RantimeAttackable = _Attackable.Value;
        _RantimeBlockable = _Blockable.Value;
        _RantimeDamagable = _Damagable.Value;
        _RantimeSerchable = _Serchable.Value;
        foreach (var init in GetComponents<ICharactorStatusInitializable>().OrderBy(x => x.Priority))
        {
            init.Initialize(_UnitData.Status);
        }
    }

    public void OperationStart()
    {
        //ダメージ受けた時の処理
        _RantimeDamagable.Helth.Subscribe(x =>
        {
            //死んだら役割終了
            if (_RantimeDamagable.IsDeath == true)
            {
                _StateManager.State.Value = UnitState.Die;
                OperationFinish();
            }
        });

        //ブロックした時の処理
        _RantimeBlockable.BlockSubject.Subscribe(_ =>
        {
            //攻撃中じゃなかったら攻撃開始
            if (_AttackTask.Status == UniTaskStatus.Succeeded)
            {
                _AttackTask = BlockAttackTask(_DestroyCancellationTokenSource.Token);
            }
        });

        //索敵
        _RantimeSerchable.ObserveEveryValueChanged(serch => serch.SerchObjects)
            .DistinctUntilChanged()
            .Subscribe(enemies =>
            {
                _SerchEnemies = enemies;
            })
            .AddTo(gameObject);

        LongRangeAttack(_DestroyCancellationTokenSource.Token);
    }

    void OnDestroy() { _DestroyCancellationTokenSource.Cancel(); }

    public void OperationStop()
    {
        _RantimeAttackable.AttackStop();
    }

    public void OperationRestart()
    {
        _RantimeAttackable.AttackReStart();
    }

    public void OperationFinish()
    {
        _OperationFinishSubject.OnNext(Unit.Default);
        Destroy(gameObject);
    }

    public IObservable<Unit> OperationFinishObservable() => _OperationFinishSubject;
}