﻿
using System;
using UniRx;
/// <summary>
/// ダメージ受けたりさせる奴
/// </summary>
public interface IDamagable
{
    IObservable<int> Helth { get; }
    bool IsDeath { get; }
    void ApplyDamage(DamageStatus damage);
}
