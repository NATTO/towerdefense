﻿using System.Collections;
using UnityEngine;

public class UnitSkill : MonoBehaviour,ISkillable,IUnitStatusInitializable
{
    [SerializeField, ReadOnly] ScriptablePlayerSkill _Skill;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        _Skill = Instantiate(((ScriptableUnitStatus)status).PlayerSkill);
    }

    public bool IsSkillActivation => true;

    public void SkillActivate()
    {

    }
}