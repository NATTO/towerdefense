using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
/// <summary>
/// ユニット置く場所
/// </summary>
public class UnitStand : MonoBehaviour
{
    [SerializeField]
    UnitAppropriate _Appropriate; 
    [SerializeField]
    Transform _UnitPosition;
    [SerializeField]
    GameObject _Unit;
    public GameObject Unit => _Unit;
    IDisposable _UnitDeathDisposable;

    public void SetUnit(Transform unitTransform, GameObject unit)
    {
        unitTransform.position = _UnitPosition.position;
        _Unit = unit;
        ICharactorControllable controlle = unit.GetComponent<ICharactorControllable>();
        _UnitDeathDisposable = controlle.OperationFinishObservable().First().Subscribe(_ => RemoveUnit());
    }

    public void RemoveUnit()
    {
        _Unit = null;
        if (_UnitDeathDisposable != null)
        {
            _UnitDeathDisposable.Dispose();
            _UnitDeathDisposable = null;
        }
    }
}