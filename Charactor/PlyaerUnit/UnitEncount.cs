﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;
/// <summary>
/// ユニットの接敵
/// </summary>
public class UnitEncount : MonoBehaviour, IUnitStatusInitializable, IBlockable
{
    //ブロック限界
    [SerializeField, ReadOnly]
    int _BlockLimit;
    //ブロック数
    [SerializeField, ReadOnly]
    int _BlockCount;
    public bool IsBlock => _BlockLimit > _BlockCount;
    //ブロックしてる敵
    [SerializeField, ReadOnly]
    Dictionary<GameObject,IEnemyUsable> _BlockEnemies = new Dictionary<GameObject, IEnemyUsable>();
    public IDictionary<GameObject, IEnemyUsable> BlockEnemies => _BlockEnemies;
    //ブロックした時の通知
    Subject<Unit> _BlockSubject = new Subject<Unit>();
    public Subject<Unit> BlockSubject => _BlockSubject;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        var rantimeStatus = (ScriptableUnitStatus)status;
        _BlockLimit = rantimeStatus.BlockLimit;
    }

    public void Block(GameObject blockUnit)
    {
        if (_BlockEnemies.ContainsKey(blockUnit))
            return;
        _BlockEnemies.Add(blockUnit,blockUnit.GetComponent<IEnemyUsable>());
        _BlockCount = _BlockEnemies.Count;
        _BlockSubject.OnNext(Unit.Default);
    }
}