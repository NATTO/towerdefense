﻿/// <summary>
/// キャラクター用初期化インターフェース
/// </summary>
public interface ICharactorStatusInitializable
{
    int Priority => 0;
    void Initialize(ScriptableBaseCharactorStatus status);
}