﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
/// <summary>
/// キャラクターが攻撃するためのクラス
/// </summary>
public class CharactorAttack : MonoBehaviour, ICharactorAttackable, ICharactorStatusInitializable
{
    [SerializeField, ReadOnly]
    DamageStatus _DamageStatus;
    /// <summary>
    /// 短距離攻撃の発生時間
    /// </summary>
    [SerializeField, ReadOnly]
    protected int _ShortRangeAttackSpeed;
    /// <summary>
    /// 長距離攻撃の発生時間
    /// </summary>
    [SerializeField, ReadOnly]
    protected int _LongRangeAttackSpeed;
    /// <summary>
    /// 同時に攻撃できる数
    /// </summary>
    [SerializeField, ReadOnly]
    protected int _CanAttackCount;

    Transform _Transform;
    bool _IsAttack = true;
    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        _DamageStatus = status.DamageStatus;
        _LongRangeAttackSpeed = status.LongRangeAttackSpeed;
        _CanAttackCount = status.CanAttackCount;
        _ShortRangeAttackSpeed = status.AttackSpeed;
        _Transform = transform;
    }

    public void AttackReStart()
    {
        _IsAttack = true;
    }

    public void AttackStop()
    {
        _IsAttack = false;
    }

    /// <summary>
    /// 攻撃発生時間
    /// </summary>
    /// <param name="range"></param>
    /// <returns></returns>
    int AttackSpeed(AttackRange range)
    {
        return range switch
        {
            AttackRange.Short => _ShortRangeAttackSpeed,
            AttackRange.Long => _LongRangeAttackSpeed,
        };
    }

    bool IsAttack(bool isAttack, GameObject target) => isAttack == true && target != null;

    public async virtual UniTask AttackTask(KeyValuePair<GameObject, IDamagable> attackEnemy, CancellationToken cancellationToken, AttackRange attackRange)
    {
        //攻撃出来なかったら帰る
        if (IsAttack(_IsAttack, attackEnemy.Key) == false) return;
        //発生まで待つ
        await UniTask.Delay(AttackSpeed(attackRange), false, PlayerLoopTiming.Update, cancellationToken);
        //待ってる間に敵消えてたら帰る
        if (IsAttack(_IsAttack, attackEnemy.Key) == false) return;

        Attack(attackEnemy, attackRange);
    }


    public async virtual UniTask AttackTask(List<KeyValuePair<GameObject, IDamagable>> attackTargets, CancellationToken cancellationToken, AttackRange attackRange)
    {
        if (_IsAttack == false) return;
        //発生まで待つ
        await UniTask.Delay(AttackSpeed(attackRange), false, PlayerLoopTiming.Update, cancellationToken);
        
        Attack(attackTargets, attackRange);
    }

    /// <summary>
    /// 攻撃処理
    /// </summary>
    /// <param name="attackTargets"></param>
    /// <param name="range"></param>
    protected virtual void Attack(IList<KeyValuePair<GameObject, IDamagable>> attackTargets, AttackRange range)
    {
        int canAttackCount = 0;        
        foreach (var target in attackTargets)
        {
            //敵いなかったら回す
            if (target.Key == null) continue;
            //攻撃対象人数超えたら帰る
            if (canAttackCount >= _CanAttackCount) break;
            Attack(target, range);
            canAttackCount++;
        }
    }

    /// <summary>
    /// 攻撃処理
    /// </summary>
    /// <param name="attackTargets"></param>
    /// <param name="range"></param>
    protected virtual void Attack(KeyValuePair<GameObject, IDamagable> attackTarget, AttackRange range)
    {
        ApplyDamage(attackTarget.Value);
    }

    void ApplyDamage(IDamagable damagable)
    {
        damagable.ApplyDamage(_DamageStatus);
    }
}