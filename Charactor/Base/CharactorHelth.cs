﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;

public class CharactorHelth : MonoBehaviour, IDamagable, ICharactorStatusInitializable
{
    const int _MinDamage = 1;
    [SerializeField]
    ReactiveProperty<int> _Helth;
    [SerializeField]
    DamageStatus _DamageStatus;
    public IObservable<int> Helth => _Helth;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        _Helth = new UniRx.ReactiveProperty<int>(status.HP);
        _DamageStatus = status.DamageStatus;
    }

    public virtual void ApplyDamage(DamageStatus status)
    {
        //HP減らす
        _Helth.Value -= status.AttackTypeAttackType switch
        {
            //魔法なら防御力貫通
            AttackType.Magical => Mathf.Clamp(status.AttackPower, _MinDamage, _Helth.Value),
            
            AttackType.Physical => Mathf.Clamp(status.AttackPower - _DamageStatus.Defence, _MinDamage, _Helth.Value)
        };
    }

    public bool IsDeath => _Helth.Value == 0;
}