﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public interface ICharactorAttackable
{
    /// <summary>
    /// 攻撃処理
    /// </summary>
    /// <param name="attackEnemy"></param>
    /// <param name="cancellationToken"></param>
    /// <param name="attackRange"></param>
    /// <returns></returns>
    UniTask AttackTask(KeyValuePair<GameObject,IDamagable> attackEnemy, CancellationToken cancellationToken,AttackRange attackRange);
    /// <summary>
    /// 攻撃処理
    /// </summary>
    /// <param name="attackEnemies"></param>
    /// <param name="cancellationToken"></param>
    /// <param name="attackRange"></param>
    /// <returns></returns>
    UniTask AttackTask(List<KeyValuePair<GameObject, IDamagable>> attackEnemies, CancellationToken cancellationToken,AttackRange attackRange);

    /// <summary>
    /// 攻撃を止める
    /// </summary>
    void AttackStop();

    /// <summary>
    /// 攻撃再開
    /// </summary>
    void AttackReStart();
}

/// <summary>
/// 攻撃範囲の種類
/// </summary>
public enum AttackRange
{
    Short,
    Long,
}
