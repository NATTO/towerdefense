using UnityEngine;
/// <summary>
/// �L�����N�^�[�̃f�[�^
/// </summary>
public class ScriptableBaseCharactorStatus : ScriptableObject
{
    [SerializeField]
    string _Name;
    public string Name { get => _Name; set { _Name = value; } }

    [SerializeField, Tooltip("�U���̔������ԁ@1000=1�b")]
    int _AttackTime;
    public int AttackTime { get => _AttackTime; set => _AttackTime = value; }

    [SerializeField, Tooltip("�U���̃��L���X�g���� 1000=1�b")]
    int _AttackSpeed;
    public int AttackSpeed { get => _AttackSpeed; set => _AttackSpeed = value; }


    [SerializeField, Tooltip("�����ɍU���ł���G�̐�")]
    int _CanAttackCount;
    public int CanAttackCount { get => _CanAttackCount; set => _CanAttackCount = value; }

    [SerializeField, Tooltip("���G����")]
    float _SerchDistance;
    public float SerchDistance { get => _SerchDistance; set => _SerchDistance = value; }

    [SerializeField]
    int _LongRangeAttackSpeed;

    public int LongRangeAttackSpeed { get => _LongRangeAttackSpeed; set => _LongRangeAttackSpeed = value; }
    [SerializeField]
    int _LongRangeAttackTime;
    public int LongRangeAttackTime { get => _LongRangeAttackTime; set => _LongRangeAttackTime = value; }

    [SerializeField]
    DamageStatus _DamageStatus;
    public DamageStatus DamageStatus { get => _DamageStatus; set => _DamageStatus = value; }

    [SerializeField]
    int _HP;
    public int HP { get => _HP; set => _HP = value; }
}
