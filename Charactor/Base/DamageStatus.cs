﻿using System;
using UnityEngine;

/// <summary>
/// ダメージにかかわるステータス
/// </summary>
[Serializable]
public struct DamageStatus
{
    [SerializeField]
    int _Defence;
    public int Defence { get => _Defence; set => _Defence = value; }

    [SerializeField]
    int _AttackPower;
    public int AttackPower { get => _AttackPower; set => _AttackPower = value; }

    [SerializeField]
    AttackType _AttackType;
    public AttackType AttackTypeAttackType { get => _AttackType;set => _AttackType = value; }
}

[Serializable]
public enum AttackType
{
    Physical,
    Magical
}