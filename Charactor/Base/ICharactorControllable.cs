﻿
using System;
/// <summary>
/// キャラクター操作するインターフェース
/// </summary>
public interface ICharactorControllable
{
    void Initialize();
    void OperationStart();
    void OperationStop();
    void OperationRestart();
    void OperationFinish();
    bool IsOperationEnd { get; }
    IObservable<UniRx.Unit> OperationFinishObservable();
}