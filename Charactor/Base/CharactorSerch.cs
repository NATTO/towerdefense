﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 索敵クラス
/// </summary>
public class CharactorSerch : MonoBehaviour, ISerchable, ICharactorStatusInitializable
{
    //索敵対象のタグ
    [TagSelecter, SerializeField]
    string _SerchTag;

    float _SerchDistance;
    public float SerchDistance => _SerchDistance;
    Transform _Transform;
    public string SerchTag => _SerchTag;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        _SerchDistance = status.SerchDistance;
        _Transform = transform;
    }

    public IEnumerable<GameObject> SerchObjects
    {
        get
        {
            if (_SerchDistance <= 0) return null;
            var result = GameObject.FindGameObjectsWithTag(_SerchTag).
                Where(obj => (obj.transform.position - _Transform.position).sqrMagnitude < _SerchDistance * _SerchDistance);
            return result.Any() ? result : null;
        }
    }
}