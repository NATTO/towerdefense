﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
/// <summary>
/// エンカウントするやつ
/// </summary>
public class ShortEncount : MonoBehaviour, IEncountable
{
    GameObject _EncountUnit;
    Subject<bool> _IsEncountSubject = new Subject<bool>();
    UnitStand _UnitStand = null;
    GameObject IEncountable.EncountUnit => _EncountUnit;
    public IObservable<bool> IsEncountObservable() => _IsEncountSubject;


    private void OnTriggerEnter(Collider other)
    {
        //ユニットが置かれてるやつに振れる
        if (other.CompareTag(TagNames.UnitStand))
        {
            _EncountUnit = null;
            _UnitStand = other.GetComponent<UnitStand>();
            _EncountUnit = _UnitStand.Unit;
            //ユニットが置かれてたら
            if (_EncountUnit != null)
            {
                //ブロックさせる
                IBlockable blockable = _EncountUnit.GetComponent<IBlockable>();
                if (blockable.IsBlock == true)
                {
                    blockable.Block(gameObject);
                    _IsEncountSubject.OnNext(true);
                }
            }
        }
    }
}