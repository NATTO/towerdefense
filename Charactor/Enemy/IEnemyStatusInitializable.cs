﻿/// <summary>
/// 敵用の初期化インターフェース
/// 敵であることの証明
/// </summary>
public interface IEnemyStatusInitializable  : ICharactorStatusInitializable
{

}
