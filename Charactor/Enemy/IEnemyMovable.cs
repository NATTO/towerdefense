using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public interface IEnemyMovable
{
    float MoveSpeed { get; }
    void MoveStart(CancellationToken cancellationToken);
    void MoveReStart();
    void MoveStop();
    void MoveStopOnTime(float stopTime);

    bool IsMoveEnd { get; }
}
