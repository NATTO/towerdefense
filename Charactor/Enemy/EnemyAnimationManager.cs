﻿using System.Collections;
using UnityEngine;
using UniRx;

/// <summary>
/// 敵のアニメーションの基本
/// </summary>
public class EnemyAnimationManager : MonoBehaviour, IEnemyStatusInitializable
{
    //ステート管理
    [SerializeField]
    EnemyStateManager _StateManager;
    [SerializeField] Animator _Animator;

    public int Priority => 1;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        //ステート管理する奴に変更が入ったらそのステート用のアニメーション流す
        _StateManager.State.Subscribe(state =>
        {
            _Animator.Play(state.ToString(),0,0);
        });
    }
}