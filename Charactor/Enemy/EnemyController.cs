﻿using System.Collections;
using UnityEngine;
using UniRx;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Linq;
using System.Threading;
using System;
using TNRD;
/// <summary>
/// 敵管理
/// </summary>
public class EnemyController : MonoBehaviour, ICharactorControllable, IEnemyUsable
{
    [SerializeField] EnemyData _EnemyData;
    [SerializeField] SerializableInterface<IEnemyMovable> _Move;
    [SerializeField] SerializableInterface<IEncountable> _Encount;
    [SerializeField] SerializableInterface<ICharactorAttackable> _Attack;
    [SerializeField] SerializableInterface<IDamagable> _Helth;
    [SerializeField] SerializableInterface<ISerchable> _Serch;
    [SerializeField] EnemyStateManager _StateManager;
    protected IEnemyMovable _RantimeMove;
    protected IEncountable _RantimeEncount;
    protected ICharactorAttackable _RantimeCharactorAttack;
    protected IDamagable _RantimeHelth;
    protected ISerchable _RantimeSerch;

    CancellationTokenSource _ControlleCancellationToken = new CancellationTokenSource();
    protected GameObject _EncountUnit;
    IEnumerable<GameObject> _SerchEnemies;
    protected Subject<Unit> _OperationFinishSubject;

    public bool IsOperationEnd => _RantimeMove.IsMoveEnd;

    public IEnemyMovable Move => _RantimeMove;

    public IEncountable Encount => _RantimeEncount;

    public ICharactorAttackable CharactorAttack => _RantimeCharactorAttack;

    public IDamagable Helth => _RantimeHelth;

    public ISerchable Serch => _RantimeSerch;

    public void Initialize()
    {
        _OperationFinishSubject = new Subject<Unit>();
        _RantimeMove = _Move.Value;
        _RantimeEncount = _Encount.Value;
        _RantimeCharactorAttack = _Attack.Value;
        _RantimeHelth = _Helth.Value;
        _RantimeSerch = _Serch.Value;
        foreach (var init in GetComponents<ICharactorStatusInitializable>().OrderBy(init => init.Priority))
        {
            init.Initialize(_EnemyData.Status);
        }
    }

    void OnDestroy()
    {
        _ControlleCancellationToken.Cancel();
    }
    /// <summary>
    /// 役割のおわり
    /// </summary>
    public void OperationFinish()
    {
        //終わりを通知
        _OperationFinishSubject.OnNext(Unit.Default);
        //動いてるタスク前停止
        _ControlleCancellationToken.Cancel();
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 役割の一時停止
    /// </summary>
    public void OperationStop()
    {
        //動きを止める
        _RantimeMove.MoveStop();
        //攻撃を止める
        _RantimeCharactorAttack.AttackStop();
    }

    /// <summary>
    /// 役割の再開
    /// </summary>
    public void OperationRestart()
    {
        if (_EncountUnit == null)
        {
            _RantimeMove.MoveReStart();
        }
        _RantimeCharactorAttack.AttackReStart();
    }
    
    /// <summary>
    /// 役割の開始
    /// </summary>
    public void OperationStart()
    {
        gameObject.SetActive(true);

        _StateManager.State.Value = EnemyState.Walk;
        //動きの開始
        _RantimeMove.MoveStart(_ControlleCancellationToken.Token);

        //敵に出会ったときの処理
        _RantimeEncount.IsEncountObservable().Subscribe(isEncount =>
        {
            if (isEncount == true)
            {
                _EncountUnit = _RantimeEncount.EncountUnit;
                _RantimeMove.MoveStop();
                _StateManager.State.Value = EnemyState.Idle;
                EncountAttackTask(_ControlleCancellationToken.Token);
            }
        }).AddTo(gameObject);

        //ダメージうけたりした時の処理
        _RantimeHelth.Helth.Subscribe(hp =>
        {
            //死んでたら
            if (_RantimeHelth.IsDeath == true)
            {
                //役割のおわり
                _StateManager.State.Value = EnemyState.Die;
                OperationFinish();
            }
        }).AddTo(gameObject);

        //移動が終わったら役割のおわり
        _RantimeMove.ObserveEveryValueChanged(move => move.IsMoveEnd)
            .Where(isMoveEnd => isMoveEnd)
            .First()
            .Subscribe(_ => OperationFinish())
            .AddTo(gameObject);

        //索敵ができたら
        _RantimeSerch.ObserveEveryValueChanged(serch => serch.SerchObjects)
            .DistinctUntilChanged()
            .Subscribe(enemies =>
            {
                //索敵した敵を入れる
                _SerchEnemies = enemies;
            })
            .AddTo(gameObject);

        LongRangeAttack(_ControlleCancellationToken.Token);
    }


    public IObservable<Unit> OperationFinishObservable() => _OperationFinishSubject;

    //索敵した敵への攻撃
    async UniTaskVoid LongRangeAttack(CancellationToken cancellationToken)
    {
        int attackTime = 0;
        //敵を探さないなら帰る
        if (_EnemyData.Status.SerchDistance <= 0) return;
        while (true)
        {
            attackTime += (int)(Time.deltaTime * 1000);
            await UniTask.Yield(cancellationToken);
            if (_SerchEnemies == null || _EncountUnit != null) continue;
            if (_SerchEnemies.Count() == 0) continue;

            _StateManager.State.Value = EnemyState.LongRangeAttack;
            //攻撃中は止まる
            _RantimeMove.MoveStop();
            await _RantimeCharactorAttack.AttackTask(_SerchEnemies.Select(enemy => new KeyValuePair<GameObject, IDamagable>(enemy, enemy.GetComponent<IDamagable>())).ToList(), cancellationToken, AttackRange.Long);
            attackTime = 0;
            _StateManager.State.Value = EnemyState.Walk;
            //終われば歩く
            _RantimeMove.MoveReStart();
        }
    }

    //ぶつかった敵への攻撃
    async UniTaskVoid EncountAttackTask(CancellationToken cancellationToken)
    {
        IDamagable encountUnitHelth = _EncountUnit.GetComponent<IDamagable>();
        while (true)
        {
            await UniTask.Delay(_EnemyData.Status.AttackTime, false, PlayerLoopTiming.Update, cancellationToken);

            _StateManager.State.Value = EnemyState.ShortRangeAttack;
            await _RantimeCharactorAttack.AttackTask(new KeyValuePair<GameObject, IDamagable>(_EncountUnit,encountUnitHelth), cancellationToken, AttackRange.Short);
            //敵が居なくなったり死んだりしたら歩く
            if (encountUnitHelth.IsDeath == true || _EncountUnit == null)
            {
                _StateManager.State.Value = EnemyState.Walk;
                _RantimeMove.MoveReStart();
                break;
            }
            _StateManager.State.Value = EnemyState.Idle;
        }
        _EncountUnit = null;
    }
}