using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UniRx;
using UnityEngine;

/// <summary>
/// 敵の移動クラス
/// </summary>
public class EnemyMove : MonoBehaviour, IEnemyMovable,IEnemyStatusInitializable
{
    //経路
    [SerializeField,ReadOnly]
    ScriptablePathTokenEnemy _PathTokenEnemy;

    [SerializeField,ReadOnly]
    float _MoveSpeed;

    Transform _Transform;

    [SerializeField, ReadOnly]
    bool _IsMove;

    float IEnemyMovable.MoveSpeed { get => _MoveSpeed; }

    bool _IsMoveEnd = false;

    public void Initialize(ScriptableBaseCharactorStatus status)
    {
        var rantimeStatus = (ScriptableEnemyStatus)status;
        _PathTokenEnemy = rantimeStatus.PathToken;
        _MoveSpeed = rantimeStatus.MoveSpeed;
        _Transform = transform;
        _IsMove = true;
        _IsMoveEnd = false;
    }

    /// <summary>
    /// 動く進行タスク
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTaskVoid MoveProgressTask(CancellationToken cancellationToken)
    {
        _Transform.position = _PathTokenEnemy._StartPosition._Position;
        
        await UniTask.Delay((int)_PathTokenEnemy._StartPosition._WaitTime * 1000, false, PlayerLoopTiming.Update, cancellationToken);

        foreach (var pathToken in _PathTokenEnemy._OnTheWayPositions)
        {
            await MoveTask(pathToken._Position, cancellationToken);

            await UniTask.Delay((int)pathToken._WaitTime * 1000, false, PlayerLoopTiming.Update, cancellationToken);
        }

        await MoveTask(_PathTokenEnemy._EndPosition._Position, cancellationToken);
        _IsMove = false;
        _IsMoveEnd = true;
    }
    /// <summary>
    /// 地点から地点へ移動
    /// </summary>
    /// <param name="toPosition">行く場所</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    async UniTask MoveTask(Vector3 toPosition, CancellationToken cancellationToken)
    {
        _Transform.LookAt(toPosition);
        while (_Transform.position != toPosition)
        {
            //移動処理
            if (_IsMove)
                _Transform.position = Vector3.MoveTowards(_Transform.position, toPosition, _MoveSpeed * TimeManager.InGameTimeScale);

            await UniTask.WaitForFixedUpdate(cancellationToken);
        }
    }
    /// <summary>
    /// 移動開始
    /// </summary>
    public void MoveStart(CancellationToken cancellationToken)
    {
        _IsMove = true;
        MoveProgressTask(cancellationToken);
    }

    /// <summary>
    /// 一時停止
    /// </summary>
    public void MoveStop()
    {
        _IsMove = false;
    }

    /// <summary>
    /// 移動再開
    /// </summary>
    public void MoveReStart()
    {
        _IsMove = true;
    }

    public void MoveStopOnTime(float stopTime)
    {
        MoveStop();
        Observable.Timer(TimeSpan.FromSeconds(stopTime)).Subscribe(_ => MoveReStart()).AddTo(this);
    }

    public bool IsMoveEnd => _IsMoveEnd;
    
}
