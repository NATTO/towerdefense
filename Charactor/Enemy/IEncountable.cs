﻿using UniRx;
using UnityEngine;
using System.Collections.Generic;
using System;

public interface IEncountable
{
    GameObject EncountUnit { get; }
    IObservable<bool> IsEncountObservable();
}