﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TNRD;
/// <summary>
/// 作る敵のデータ
/// </summary>
[CreateAssetMenu(menuName = "Enemy/GenerateEnemyData")]
public class GenerateEnemyData : ScriptableObject
{
    //作る敵
    [SerializeField]
    List<SerializableInterface<ICharactorControllable>> _Enemies;
    public List<SerializableInterface<ICharactorControllable>> Enemies => _Enemies;
    //敵を作るまでの時間
    [SerializeField]
    int _GenerateWaitTime;
    public int GenerateWaitTime => _GenerateWaitTime;
    //次のデータ
    [SerializeField]
    GenerateEnemyData _NextEnemyData;
    public GenerateEnemyData NextEnemyData { get => _NextEnemyData; set => _NextEnemyData = value; }
}