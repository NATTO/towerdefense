﻿using System.Collections;
using UniRx;
using UnityEngine;

public class EnemyStateManager : MonoBehaviour
{
    [SerializeField]
    ReactiveProperty<EnemyState> _State = new ReactiveProperty<EnemyState>();
    public ReactiveProperty<EnemyState> State => _State;

}

public enum EnemyState
{
    Idle,
    Walk,
    ShortRangeAttack,
    LongRangeAttack,
    Die
}