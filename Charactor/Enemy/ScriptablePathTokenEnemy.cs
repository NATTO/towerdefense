using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// �G���ʂ�o�H
/// </summary>
[CreateAssetMenu(menuName = "Enemy/PathTokenEnemy")]
public class ScriptablePathTokenEnemy : ScriptableObject
{

    public TokenEnemy _StartPosition;

    public TokenEnemy _EndPosition;

    public TokenEnemy[] _OnTheWayPositions;
}
