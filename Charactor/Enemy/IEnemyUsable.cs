﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IEnemyUsable
{
    IEnemyMovable Move { get; }
    IEncountable Encount { get; }
    ICharactorAttackable CharactorAttack { get; }
    IDamagable Helth { get; }
    ISerchable Serch { get; }
}