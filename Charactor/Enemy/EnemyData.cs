using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    [SerializeField]
    ScriptableEnemyStatus _Status;

    public ScriptableEnemyStatus Status => _Status;
}
