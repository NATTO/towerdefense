﻿using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// 敵のステータス
/// </summary>
[CreateAssetMenu(menuName = "Enemy/Status")]
public class ScriptableEnemyStatus : ScriptableBaseCharactorStatus
{
    [SerializeField]
    float _MoveSpeed;
    public float MoveSpeed { get { return _MoveSpeed; } set { _MoveSpeed = value; } }

    [SerializeField]
    ScriptablePathTokenEnemy _PathToken;
    public ScriptablePathTokenEnemy PathToken { get  => _PathToken; set { _PathToken = value; } }
}